## 1 运行项目

### 安装 nvm

`curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.5/install.sh | bash`或

```
git clone https://gitee.com/mirrors/nvm
cd nvm
bash install.sh
```

### 安装并设置 node

```
nvm install v16
nvm alias default 16
# 或者nvm use 16
```

### 安装依赖

`npm install`

### 运行

`npm run start`

## 2 问题处理

### 端口被占用

```
lsof -i:3000
kill -9 PID号
```
