// 检验IP地址
function isValidIP(ip) {
	const reg = /^(\d{1,2}|1\d\d|2[0-4]\d|25[0-5])\.(\d{1,2}|1\d\d|2[0-4]\d|25[0-5])\.(\d{1,2}|1\d\d|2[0-4]\d|25[0-5])\.(\d{1,2}|1\d\d|2[0-4]\d|25[0-5])$/
	return reg.test(ip);
}
// 检验端口号
function isValidPort(port) {
	const num = Number(port);
	return num >= 1024 && num <= 65535;
}
export { isValidIP, isValidPort };