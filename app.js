const express = require("express");
const logger = require("morgan");

const indexRouter = require("./routes/index");

const app = express();
app.use(logger("dev"));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use("/static", express.static("public"));
app.use("/", indexRouter);

app.listen(3001, () => {
  console.log(`express server listen at http://localhost:3001`);
});
